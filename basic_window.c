#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int x, y, width, height, border;
	Display *disp;
	int screen;
	Window wind;

	/* try experimenting with different top-left coordinates */
	x = y = 0;

	/* try experimenting with different widths and heights */
	width = height = 500;

	/* try experimenting with different border widths */
	border = 0;

	if (!(disp = XOpenDisplay(NULL)))
	{
		fprintf(stderr, "Could not open an X display.\n");
		return EXIT_FAILURE;
	}

	/* get the default screen of the now-opened display */
	screen = DefaultScreen(disp);

	wind = XCreateSimpleWindow(disp, RootWindow(disp, screen),
			x, y, /* the upper left start coordinates */
			width, height, /* the dimensions of the window */
			0, /* the size of window borders - 0 for none */
			BlackPixel(disp, screen), WhitePixel(disp, screen));

	/* map our new window to the display and flush it */
	XMapWindow(disp, wind);
	XFlush(disp);

	/* sleep for a few seconds before cleaning up and exiting */
	sleep(5);

	/* destroy the window and close the display to avoid memory leaks */
	XDestroyWindow(disp, wind);
	XCloseDisplay(disp);

	return EXIT_SUCCESS;
}

